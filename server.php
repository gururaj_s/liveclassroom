<?php
set_time_limit(0);

$filename = 'data.txt';

while (true)
{
    $poll_timestamp = isset($_GET['timestamp']) ? (int)$_GET['timestamp'] : null;
    clearstatcache();
    $change_timestamp = filemtime($filename);
    if (($poll_timestamp == null) || ($change_timestamp > $poll_timestamp))
    {
        $data = file_get_contents($filename);
        $result = array(
                        'data' => $data,
                        'timestamp' => $change_timestamp
                       );
        $json = json_encode($result);
        echo $json;
        break;

    }
    else
    {
        sleep(1);
        continue;
    }
}
