<?php
set_time_limit(0);

$filename = 'state.txt';

while (true)
{
    $poll_timestamp = isset($_GET['timestamp']) ? (int)$_GET['timestamp'] : null;
    clearstatcache();
    $change_timestamp = filemtime($filename);
    if (($poll_timestamp == null) || ($change_timestamp > $poll_timestamp))
    {
        $data = file_get_contents($filename);
        $elems = explode("&", $data);
        $result = array(
                        'mode' => $elems[0],
                        'page' => $elems[1],
                        'timestamp' => $change_timestamp
                       );
        $json = json_encode($result);
        echo $json;
        break;

    }
    else
    {
        sleep(1);
        continue;
    }
}
