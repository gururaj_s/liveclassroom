function getContent(timestamp)
{
    var queryString = {'timestamp' : timestamp};

    $.ajax(
        {
            type: 'GET',
            url: 'http://192.168.1.114:8888/server.php',
            data: queryString,
            success: function(data){
                var obj = jQuery.parseJSON(data);
                var pre = "pdf.pdf#page=";
                var post = "&view=FitV&scrollbar=0&toolbar=0&statusbar=0&messages=0&navpanes=0";
                var out = pre.concat(obj.data, post);
                $('#response').html(out);
                $('#pdf_js_display').attr("data", out);
                getContent(obj.timestamp);
            }
        }
    );
}

$(function() {
    getContent();
});
