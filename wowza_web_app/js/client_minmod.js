function getContent(timestamp)
{
    var queryString = {'timestamp' : timestamp};

    $.ajax(
        {
            type: 'GET',
            url: 'http://192.168.1.114:8888/server1.php',
            data: queryString,
            success: function(data){
                var obj = jQuery.parseJSON(data);
		if(obj.mode == 0){
		    $('#pdf_frame').attr("style","display:none");
		    $('#response').html("Mode: Video");
		}   else {
		    $('#pdf_frame').attr("style","display:block");
		    var pre = "pdf.pdf#page=";
                    var post = "&view=FitV&scrollbar=0&toolbar=0&statusbar=0&messages=0&navpanes=0";
                    var out = pre.concat(obj.page, post);
                    $('#pdf_js_display').attr("data", out);
                    $('#response').html("Mode:PDF, PDF URL is: "+out);
		}
                getContent(obj.timestamp);
            }
        }
    );
}

$(function() {
    getContent();
});
