# LiveClassroom
Authors: Pushkar Godbole, Vinit Deodhar, Gururaj Saileshwar

This is the repository of the LiveClassroom android app. It was our entry in HackGT 2015 (Georgia Tech's annual Hackathon).

This app enables live streaming of Classroom Lectures with seemless switching between slides and a document view, from an android phone.
The camera of the phone mounted on a selfie-stick doubles down as a document camera.
The stream is hosted on a Wowza web server and can be viewed from a browser.

Presently, the app enables:
* Seemless switching between pdf slides and camera stream from the phone
* Slide switching for the pdf
* Support for multiple clients to view the live lecture stream via a browser

The streaming component of the app uses the Libstreameing android library developed by github.com/fyhertz.

Further enhancements to the app would include:
* Taking snapshots of the document while switching to slides
* Interleaving the snapshots with the slides to create a final pdf to mimic a coherent progression of the lecture
* Auto hosting the slides on the server at the end of the lecture
